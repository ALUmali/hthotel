-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 31, 2018 at 09:37 PM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hthotel_hometown_hotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `addons_masterfile`
--

CREATE TABLE `addons_masterfile` (
  `Addon_ID` int(11) NOT NULL,
  `Addon_name` varchar(50) NOT NULL,
  `Addon_rate` int(11) NOT NULL,
  `Addon_description` varchar(50) NOT NULL,
  `Addon_status` varchar(50) NOT NULL,
  `Discount_Id` int(11) NOT NULL,
  `Addon_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addons_masterfile`
--

INSERT INTO `addons_masterfile` (`Addon_ID`, `Addon_name`, `Addon_rate`, `Addon_description`, `Addon_status`, `Discount_Id`, `Addon_qty`) VALUES
(1, 'Extra Bed', 400, 'None', 'Available', 9, 15),
(2, 'Extra Pillow', 100, 'None', 'Available', 9, 15),
(4, 'Shuttle service', 150, 'None', 'Available', 9, 1),
(5, 'Extra Sheet', 100, '', 'Available', 9, 15);

-- --------------------------------------------------------

--
-- Table structure for table `adminuser_masterfile`
--

CREATE TABLE `adminuser_masterfile` (
  `user_id` int(11) NOT NULL,
  `User_firstname` varchar(255) NOT NULL,
  `User_lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `admin_type` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminuser_masterfile`
--

INSERT INTO `adminuser_masterfile` (`user_id`, `User_firstname`, `User_lastname`, `email`, `admin_type`, `password`) VALUES
(2, 'aaaa', 'aaaaaa', 'aaaa@gmail.com', 'Admin', '$2y$10$xuEbO5WsD6B5TkOLrzlObOhPt2I7YnSS/GLwq6W50YclZKz15iXOS'),
(14, 'admin', 'admin', 'admin@admin.com', 'Admin', '$2y$10$E5Vf4KZxZMlXJRo3VN9Vz.8LT6HkwlQdFOgE.4GRr4qJrbfMHVtfq'),
(15, 'front', 'desk', 'frontdesk@gmail.com', 'FrontDesk', '$2y$10$BCAH9MQSdRtIor3cp2nYNO60/lUEg.emvrnoyVg/9lqubnM8jmRUm');

-- --------------------------------------------------------

--
-- Table structure for table `assignedroom_masterfile`
--

CREATE TABLE `assignedroom_masterfile` (
  `room_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `assignedroom_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `code` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignedroom_masterfile`
--

INSERT INTO `assignedroom_masterfile` (`room_id`, `date`, `assignedroom_id`, `status`, `type`, `code`) VALUES
(33, '2018-02-27', 1214, 'Temporary', 'Reservation', 'HUSTLKB1DI'),
(33, '2018-02-28', 1215, 'Temporary', 'Reservation', 'HUSTLKB1DI'),
(34, '2018-02-27', 1216, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(34, '2018-02-28', 1217, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(35, '2018-02-27', 1218, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(35, '2018-02-28', 1219, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(36, '2018-02-27', 1220, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(36, '2018-02-28', 1221, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(37, '2018-02-27', 1222, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(37, '2018-02-28', 1223, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(38, '2018-02-27', 1224, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(38, '2018-02-28', 1225, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(39, '2018-02-27', 1226, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(39, '2018-02-28', 1227, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(40, '2018-02-27', 1228, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(40, '2018-02-28', 1229, 'Reserved', 'Reservation', '7BQQ8S0IVA'),
(33, '2018-03-01', 1230, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(33, '2018-03-02', 1231, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(34, '2018-03-01', 1232, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(34, '2018-03-02', 1233, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(35, '2018-03-01', 1234, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(35, '2018-03-02', 1235, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(36, '2018-03-01', 1236, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(36, '2018-03-02', 1237, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(37, '2018-03-01', 1238, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(37, '2018-03-02', 1239, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(38, '2018-03-01', 1240, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(38, '2018-03-02', 1241, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(39, '2018-03-01', 1242, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(39, '2018-03-02', 1243, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(40, '2018-03-01', 1244, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(40, '2018-03-02', 1245, 'Reserved', 'Reservation', 'UN1QU49FIU'),
(44, '2018-02-27', 1250, 'Temporary', 'Reservation', '9JA700HPOC'),
(44, '2018-02-28', 1251, 'Temporary', 'Reservation', '9JA700HPOC'),
(41, '2018-02-28', 1252, 'Reserved', 'Walkin', 'Y5YBPCC0FX'),
(41, '2018-03-01', 1253, 'Reserved', 'Walkin', 'Y5YBPCC0FX'),
(42, '2018-02-28', 1254, 'Reserved', 'Walkin', 'Y5YBPCC0FX'),
(42, '2018-03-01', 1255, 'Reserved', 'Walkin', 'Y5YBPCC0FX'),
(43, '2018-02-28', 1256, 'Reserved', 'Walkin', 'Y5YBPCC0FX'),
(43, '2018-03-01', 1257, 'Reserved', 'Walkin', 'Y5YBPCC0FX'),
(41, '2018-03-02', 1258, 'Reserved', 'Reservation', 'NVT9XDOTCZ'),
(41, '2018-03-03', 1259, 'Reserved', 'Reservation', 'NVT9XDOTCZ'),
(45, '2018-02-27', 1260, 'Reserved', 'Reservation', 'SZ26LG2OCF'),
(45, '2018-02-28', 1261, 'Reserved', 'Reservation', 'SZ26LG2OCF'),
(46, '2018-02-27', 1262, 'Temporary', 'Reservation', 'B9FT5T1RYZ'),
(46, '2018-02-28', 1263, 'Temporary', 'Reservation', 'B9FT5T1RYZ'),
(33, '2018-06-24', 1264, 'Temporary', 'Reservation', '6G2UDQY9QS'),
(33, '2018-06-25', 1265, 'Temporary', 'Reservation', '6G2UDQY9QS'),
(33, '2018-07-04', 1266, 'Temporary', 'Reservation', 'UHR5LLAWZS'),
(33, '2018-07-05', 1267, 'Temporary', 'Reservation', 'UHR5LLAWZS'),
(34, '2018-07-04', 1268, 'Temporary', 'Reservation', 'UHR5LLAWZS'),
(34, '2018-07-05', 1269, 'Temporary', 'Reservation', 'UHR5LLAWZS'),
(33, '2018-07-16', 1270, 'Temporary', 'Reservation', 'MKVP5EAF5V'),
(33, '2018-07-17', 1271, 'Temporary', 'Reservation', 'MKVP5EAF5V'),
(33, '2018-07-31', 1272, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(33, '2018-08-01', 1273, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(33, '2018-08-02', 1274, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(33, '2018-08-03', 1275, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(33, '2018-08-04', 1276, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(41, '2018-07-31', 1277, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(41, '2018-08-01', 1278, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(41, '2018-08-02', 1279, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(41, '2018-08-03', 1280, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(41, '2018-08-04', 1281, 'Reserved', 'Walkin', 'Y8SNSZ7M4S'),
(33, '2018-08-11', 1282, 'Reserved', 'Walkin', 'T8ZAW3X6DO'),
(33, '2018-08-12', 1283, 'Reserved', 'Walkin', 'T8ZAW3X6DO'),
(34, '2018-08-11', 1284, 'Reserved', 'Walkin', 'T8ZAW3X6DO'),
(34, '2018-08-12', 1285, 'Reserved', 'Walkin', 'T8ZAW3X6DO'),
(33, '2018-08-21', 1286, 'Temporary', 'Reservation', 'H9MJ11UHJ6'),
(33, '2018-08-22', 1287, 'Temporary', 'Reservation', 'H9MJ11UHJ6'),
(33, '2018-08-23', 1288, 'Temporary', 'Reservation', 'H9MJ11UHJ6'),
(34, '2018-08-21', 1289, 'Temporary', 'Reservation', 'H9MJ11UHJ6'),
(34, '2018-08-22', 1290, 'Temporary', 'Reservation', 'H9MJ11UHJ6'),
(34, '2018-08-23', 1291, 'Temporary', 'Reservation', 'H9MJ11UHJ6'),
(33, '2018-08-28', 1292, 'Reserved', 'Reservation', 'PMBJNALQ67'),
(33, '2018-08-29', 1293, 'Reserved', 'Reservation', 'PMBJNALQ67'),
(41, '2018-08-28', 1294, 'Temporary', 'Reservation', 'EOBY41PPR0'),
(41, '2018-08-29', 1295, 'Temporary', 'Reservation', 'EOBY41PPR0'),
(33, '2018-09-04', 1296, 'Temporary', 'Reservation', 'XJXES6B3L8'),
(33, '2018-09-05', 1297, 'Temporary', 'Reservation', 'XJXES6B3L8');

-- --------------------------------------------------------

--
-- Table structure for table `billing_masterfile`
--

CREATE TABLE `billing_masterfile` (
  `billing_id` int(11) NOT NULL,
  `guest_id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `balance` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` varchar(30) NOT NULL,
  `total` float NOT NULL,
  `downpayment` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing_masterfile`
--

INSERT INTO `billing_masterfile` (`billing_id`, `guest_id`, `reservation_id`, `balance`, `created_at`, `updated_at`, `status`, `total`, `downpayment`) VALUES
(149, 32, 178, 0, '2018-02-24 14:50:43', '2018-08-28 22:11:12', 'Fully Paid', 15600, 2340),
(150, 32, 179, 10920, '2018-02-24 14:53:39', '2018-02-24 14:54:58', 'Partial', 15600, 2340),
(151, 32, 180, 2800, '2018-02-24 14:57:11', '2018-02-24 15:02:27', 'Not paid', 2800, 420),
(152, 32, 181, 2400, '2018-02-24 15:13:21', '2018-02-24 15:13:21', 'Not paid', 2400, 360),
(153, 32, 182, 2400, '2018-02-24 15:14:20', '2018-02-24 15:14:20', 'Not paid', 2400, 360),
(155, 58, 189, 1950, '2018-08-25 15:04:51', '2018-08-25 15:04:51', 'Not paid', 1950, 292.5),
(156, 58, 190, 0, '2018-08-25 15:09:36', '2018-08-25 15:25:01', 'Fully Paid', 2400, 360),
(157, 55, 191, 2350, '2018-09-01 12:33:29', '2018-09-01 12:33:29', 'Not paid', 2350, 352.5);

-- --------------------------------------------------------

--
-- Table structure for table `discount_masterfile`
--

CREATE TABLE `discount_masterfile` (
  `discount_ID` int(11) NOT NULL,
  `discount_percent` int(11) NOT NULL,
  `discount_name` varchar(50) NOT NULL,
  `discount_description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `financialreports_masterfile`
--

CREATE TABLE `financialreports_masterfile` (
  `payment` int(11) NOT NULL,
  `payment_type` text NOT NULL,
  `created_at` datetime NOT NULL,
  `billing_id` int(11) NOT NULL,
  `financialreport_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financialreports_masterfile`
--

INSERT INTO `financialreports_masterfile` (`payment`, `payment_type`, `created_at`, `billing_id`, `financialreport_id`) VALUES
(7800, 'Partial', '2018-02-24 14:31:20', 0, 185),
(8000, 'Partial', '2018-02-24 14:31:40', 0, 186),
(15000, 'Partial', '2018-02-24 14:33:06', 0, 187),
(15000, 'Partial', '2018-02-24 14:33:22', 0, 188),
(500, 'Partial', '2018-02-24 14:33:41', 0, 189),
(100, 'Fully Paid', '2018-02-24 14:34:05', 0, 190),
(50, 'Partial', '2018-02-24 14:52:52', 149, 191),
(2340, 'Partial', '2018-02-24 14:54:08', 149, 192),
(2340, 'Partial', '2018-02-24 14:54:24', 150, 193),
(2340, 'Partial', '2018-02-24 14:54:58', 150, 194),
(400, 'Partial', '2018-08-25 15:24:42', 156, 195),
(2000, 'Fully Paid', '2018-08-25 15:25:01', 156, 196),
(10000, 'Partial', '2018-08-28 22:10:53', 149, 197),
(3210, 'Fully Paid', '2018-08-28 22:11:12', 149, 198);

-- --------------------------------------------------------

--
-- Table structure for table `guestaddons_masterfile`
--

CREATE TABLE `guestaddons_masterfile` (
  `guestaddon_id` int(11) NOT NULL,
  `addons_id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guestaddons_masterfile`
--

INSERT INTO `guestaddons_masterfile` (`guestaddon_id`, `addons_id`, `reservation_id`, `quantity`) VALUES
(169, 1, 180, 1),
(174, 1, 191, 1);

-- --------------------------------------------------------

--
-- Table structure for table `guest_masterfile`
--

CREATE TABLE `guest_masterfile` (
  `guest_ID` int(11) NOT NULL,
  `guest_firstname` varchar(50) NOT NULL,
  `guest_lastname` varchar(50) NOT NULL,
  `guest_email` varchar(200) NOT NULL,
  `guest_password` varchar(200) NOT NULL,
  `guest_contactNumber` text NOT NULL,
  `guest_country` varchar(200) NOT NULL,
  `guest_address` varchar(200) NOT NULL,
  `guest_code` varchar(10) NOT NULL,
  `count` int(11) NOT NULL,
  `type` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest_masterfile`
--

INSERT INTO `guest_masterfile` (`guest_ID`, `guest_firstname`, `guest_lastname`, `guest_email`, `guest_password`, `guest_contactNumber`, `guest_country`, `guest_address`, `guest_code`, `count`, `type`) VALUES
(24, 'Alyanna', 'Cnatos', 'alyannamariz@gmail.com', '$2y$10$2kWqT402aek/AjYSV/klAuRvCxnickNnw6pSvb9q0rZJRkeJ1qCE.', '09277359665', 'Philippines', 'qweqweqweqw', 'EBVTVX58F3', 0, 'Member'),
(26, 'Jeemar', 'Flores', 'jeemarmflores@yahoo.com', '$2y$10$BsvERlhOFgl7t7.SDvKkpOXs17RXcZc60DOi9L6uW/zMZ21IqA42.', '09451541471', 'Philippines', '439 L. Francisco St. Pasay City', 'F8YPM04FF1', 0, 'Member'),
(27, 'jerome', 'ubina', 'jeromedomubina@gmail.com', '$2y$10$oYfpAQy2x1R5AHk.Z5xesOT25qY3l97ADeAbAowE3VZbI5FY.haHu', '09357771340', 'Philippines', '2245 Tramo St. San Roque District', 'UL5RFHDSBI', 0, 'Member'),
(30, 'jerry', 'punzalan', 'jerry111@gmail.com', '$2y$10$3dbKhrH6AK7soQBBTNewDOgb0UU/4ED.DX/kqLVtVIjC11O8KWPkW', '09357771340', 'Philippines', '123', 'CYKST4SWBG', 0, 'Member'),
(31, 'Francis', 'Torculas', 'acistorculas98@gmail.com', '$2y$10$/iIMNSvTpT3qla0On9hzb.4IahipbQGKziStzr.eboYjpCiQfPeVy', '09277259665', 'Philippines', 'P2-06 1st pasau city', '347BGLSPSO', 0, 'Member'),
(32, 'Jerome', 'Dominique', 'jdom@gmail.com', '$2y$10$rLftNYp/5kXL50sDlKY7xOo3Pmrz122np.XQHpRGlyIyhiZkTnZDm', '09095483567', 'Philippines', 'Pasay City', '2HL6SL284R', 7, 'Member'),
(35, 'roselyn', 'alcantara', 'roselynalcantara@gmail.com', '$2y$10$QDpvjXjcfpGG.FbD7vaqsuu3jRxQZ2rNtTqc9/d7qyYroXgoTtJJi', '09266807692', 'Philippines', 'Quezon City', 'S7WQEL8XBD', 0, 'Member'),
(36, 'Mon', 'Mon', 'eltorres.ue@gmail.com', '$2y$10$Qf.yUbGCwCCzTHnmBinwieSvyyuenbbSCv/Owe.x48y5p9e/14axO', '12345', 'Philippines', 'Qwer', 'BD140NDZ7Q', 0, 'Member'),
(37, 'Jerzy', 'Latican', 'jerzy_latican@yahoo.com', '$2y$10$23k2lFpWkbJ2VF/PHEliYebv96SMdL4AVCYr8RdhgU46r55QZ3bhW', '09052101762', 'Philippines', 'test', '7VF2ECQA63', 0, 'Member'),
(41, 'Anton', 'Umali', 'saasd@gmail.com', '$2y$10$RQuRZ7/q10wAcEWCc693yuCQV8O1gBhyEG0bE/FzhsOMfUcmTI3Ga', '123', 'Philippines', 'Manila', 'CECZPBFHI7', 0, ''),
(42, 'Francis', 'Torculas', 'francis_pogi0@yahoo.com', '$2y$10$qv86qGfDked765gVbPWh6eJzmMMQA1uniQsl1kvgVF3CzDNLvkX46', '09277359665', 'Philippines', 'p2-06 pasay city', '5TISR954SS', 0, ''),
(43, 'Francis', 'Torculas', 'asdasd@gmail.com', '$2y$10$N3CjVyoF4sOWKROG6qkyGujhgHYO1z0dost8M28sqEydIf9XHECP2', '09277359665', '', '', 'WYAYOT7JBY', 0, ''),
(44, 'Anton', 'Umali', 'sanzo.cx@gmail.com', '$2y$10$uk01lDes5lbdd6DWAcyzCubs0vv.Tp1qsH12E4ffrVEgu8WUdYGiy', '123', 'Philippines', 'Manila', 'J04BXUUOV', 0, ''),
(49, 'Anton', 'Umali', 'antonioumali@gmail.com', '$2y$10$.PV16UsJW59s/.tF17ImC.NCml6x/vQFPsGNtzYJLUJC71LgKOe3O', '123', 'Philippines', 'manila', '83RFYH68IS', 0, ''),
(50, 'charm', 'magpantay', 'charmain.magpantay@gmail.com', '$2y$10$Ai2/nj5O3N.u0u9iwlF49.y2SqCYZCn3kfRDrNbysmU4pr/.TMihu', '2125420', 'Philippines', 'manila', '19GCCXUNTK', 0, ''),
(51, 'sadasd', 'asdasd', 'asdasdasdasdasdasd@yahoo.com', '$2y$10$OKR7meaOrlwYkbA0BU2i2.XvKctOusyz8mFwjtlF8aA0AnTMfGTba', '12345123', 'Philippines', 'asdasdasd', 'T55FBFC66K', 0, ''),
(52, 'qwe', 'qwe', 'qweqweqweqweqweqwe@yahoo.com', '$2y$10$ZLN1/K0umHaXBRH8FNf2nujeZevQztWshzrCL1kBw94bDZ6jrx3AK', '1234567777', 'Philippines', '123123213', 'M7U1TWR9J5', 0, ''),
(53, 'Omar', 'Labio', 'marolabio@gmail.com', '$2y$10$LWPqI9jINN/cQm0WipBuDuyaYqZwO4o2dGDcGNsqpq.vmwvklLj8q', '09626262722', 'USA', 'Hdhe', 'TNNLTISEW8', 0, ''),
(54, 'jerome', 'ubina', 'abc@gmail.com', '$2y$10$bDhlAz9V72VkUF5W.qcYO.yifbNju55v/yY3m345oY3GkoqEguKrm', '09095483567', 'Philippines', '146 saint francis maricaban', 'TWHPOYQ694', 0, ''),
(55, 'Anton', 'Umali', '123123@gmail.com', '$2y$10$Mhmxltg82ZAwv1SMT3B4N.L1E3keZtcUBRh8YXhwnAoepPgDNllji', '12313123', 'Philippines', 'QEQWEQEQWEWQEWQE', '9JAV7RAJ17', 0, ''),
(56, 'Julius', 'Pangilinan', 'juliuspangilinanjr@gmail.com', '$2y$10$99i0YvOwayJj7LHQNbOBVu2E/G4TB7lStF7QgNIX7iiGpzlptgzLa', '09051122436', 'Philippines', 'blk 3 lot 26, emerald st. Adelina 1-a', '3KP3EYTCOP', 0, ''),
(57, 'Julius', 'Dejon', 'juliusdejon@gmail.com', '$2y$10$w41TPsANpEf..t1fvbGySuDRQ1onyMjD0pZAUO35hOv/OAV1bDU5e', '12312312', 'Philippines', '12312', 'EQTCTFCI7Y', 0, ''),
(58, 'jeff', 'jeff', 'j@gmail.com', '$2y$10$2a6eEpPo1Gts0caZcOF8nO4RVxzR8S8owh9DM8B36jRLQYkvLl2.m', '09357771340', 'Philippines', '2245 Tramo St. San Roque District', 'R9017B27ML', 1, ''),
(59, 'Franchesca', 'Timbal', 'franchescatimbal@ymail.com', '$2y$10$AG4goTmIu5qGx6fh0E.nduO0oweccoNXos5E17sfQDzbCfbaH823i', '09361620430', 'Philippines', '', 'XS5DQZZAIK', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `proofofpayment_masterfile`
--

CREATE TABLE `proofofpayment_masterfile` (
  `proofofpayment_id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proofofpayment_masterfile`
--

INSERT INTO `proofofpayment_masterfile` (`proofofpayment_id`, `reservation_id`, `path`) VALUES
(88, 179, '../uploads/Sir Benjamin.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `receipts_masterfile`
--

CREATE TABLE `receipts_masterfile` (
  `receipts_id` int(11) NOT NULL,
  `guest_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipts_masterfile`
--

INSERT INTO `receipts_masterfile` (`receipts_id`, `guest_id`) VALUES
(7, 32),
(8, 58);

-- --------------------------------------------------------

--
-- Table structure for table `reports_masterfile`
--

CREATE TABLE `reports_masterfile` (
  `report_id` int(11) NOT NULL,
  `date_generated` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `transaction_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservationreports_masterfile`
--

CREATE TABLE `reservationreports_masterfile` (
  `reservereports_id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `type` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservationreports_masterfile`
--

INSERT INTO `reservationreports_masterfile` (`reservereports_id`, `reservation_id`, `created_at`, `updated_at`, `type`) VALUES
(68, 109, '2018-02-24 14:34:18', '2018-02-24 14:34:18', 'walkin'),
(69, 109, '2018-02-24 14:35:24', '2018-02-24 14:35:24', 'walkin'),
(70, 178, '2018-02-24 14:51:49', '2018-02-24 14:51:49', 'reserve'),
(71, 178, '2018-02-24 14:52:03', '2018-02-24 14:52:03', 'reserve'),
(72, 178, '2018-02-24 15:12:35', '2018-02-24 15:12:35', 'reserve'),
(73, 180, '2018-02-24 15:12:47', '2018-02-24 15:12:47', 'reserve'),
(74, 179, '2018-02-24 15:12:59', '2018-02-24 15:12:59', 'reserve'),
(75, 181, '2018-02-24 15:13:46', '2018-02-24 15:13:46', 'reserve'),
(76, 179, '2018-02-24 15:13:58', '2018-02-24 15:13:58', 'reserve'),
(77, 181, '2018-02-24 15:14:02', '2018-02-24 15:14:02', 'reserve'),
(78, 179, '2018-02-24 15:14:37', '2018-02-24 15:14:37', 'reserve'),
(79, 182, '2018-02-24 15:14:44', '2018-02-24 15:14:44', 'reserve'),
(80, 181, '2018-02-24 15:14:51', '2018-02-24 15:14:51', 'reserve'),
(81, 179, '2018-02-24 15:15:01', '2018-02-24 15:15:01', 'reserve'),
(82, 179, '2018-02-24 15:15:46', '2018-02-24 15:15:46', 'reserve'),
(83, 182, '2018-02-24 15:15:49', '2018-02-24 15:15:49', 'reserve'),
(84, 179, '2018-02-24 15:16:11', '2018-02-24 15:16:11', 'reserve'),
(85, 189, '2018-08-25 15:23:45', '2018-08-25 15:23:45', 'reserve'),
(86, 190, '2018-08-25 15:24:05', '2018-08-25 15:24:05', 'reserve');

-- --------------------------------------------------------

--
-- Table structure for table `reservation_masterfile`
--

CREATE TABLE `reservation_masterfile` (
  `reservation_id` int(11) NOT NULL,
  `guest_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `checkindate` date NOT NULL,
  `checkoutdate` date NOT NULL,
  `number_guest` int(11) NOT NULL,
  `room_number` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `reservation_code` varchar(10) NOT NULL,
  `discounted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation_masterfile`
--

INSERT INTO `reservation_masterfile` (`reservation_id`, `guest_id`, `room_id`, `checkindate`, `checkoutdate`, `number_guest`, `room_number`, `status`, `reservation_code`, `discounted`) VALUES
(177, 49, 17, '2018-02-27', '2018-02-28', 1, 1, 'Void', 'HUSTLKB1DI', 0),
(178, 32, 17, '2018-02-27', '2018-02-28', 1, 8, 'Checkin', '7BQQ8S0IVA', 0),
(179, 32, 17, '2018-03-01', '2018-03-02', 1, 8, 'Approved', 'UN1QU49FIU', 0),
(180, 32, 19, '2018-02-27', '2018-02-28', 1, 1, 'Checkout', '9JA700HPOC', 0),
(181, 32, 18, '2018-03-02', '2018-03-03', 1, 1, 'Checkin', 'NVT9XDOTCZ', 0),
(182, 32, 19, '2018-02-27', '2018-02-28', 1, 1, 'Checkout', 'SZ26LG2OCF', 0),
(183, 32, 19, '2018-02-27', '2018-02-28', 1, 1, 'Void', 'B9FT5T1RYZ', 0),
(184, 55, 17, '2018-06-24', '2018-06-25', 1, 1, 'Void', '6G2UDQY9QS', 0),
(185, 55, 17, '2018-07-04', '2018-07-05', 1, 2, 'Void', 'UHR5LLAWZS', 0),
(186, 56, 17, '2018-07-16', '2018-07-17', 1, 1, 'Void', 'MKVP5EAF5V', 0),
(187, 56, 21, '2018-07-24', '2018-07-25', 1, 1, 'Void', 'D3B0WW36A0', 0),
(188, 57, 17, '2018-08-21', '2018-08-23', 3, 2, 'Void', 'H9MJ11UHJ6', 0),
(189, 58, 17, '2018-08-28', '2018-08-29', 1, 1, 'Approved', 'PMBJNALQ67', 0),
(190, 58, 18, '2018-08-28', '2018-08-29', 1, 1, 'Checkout', 'EOBY41PPR0', 0),
(191, 55, 17, '2018-09-04', '2018-09-05', 1, 1, 'Pending', 'XJXES6B3L8', 0);

-- --------------------------------------------------------

--
-- Table structure for table `room_masterfile`
--

CREATE TABLE `room_masterfile` (
  `room_id` int(11) NOT NULL,
  `room_type` varchar(50) NOT NULL,
  `room_description` varchar(50) NOT NULL,
  `room_capacity` varchar(50) NOT NULL,
  `room_rate` float NOT NULL,
  `room_number` int(11) NOT NULL,
  `room_status` varchar(50) NOT NULL,
  `room_imagepath` text NOT NULL,
  `addon_capacity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_masterfile`
--

INSERT INTO `room_masterfile` (`room_id`, `room_type`, `room_description`, `room_capacity`, `room_rate`, `room_number`, `room_status`, `room_imagepath`, `addon_capacity`) VALUES
(17, 'Twin Queen Room', 'Capacity: 2 pax', '2', 1950, 8, 'Available', 'img/Twin.JPG', 2),
(18, 'Quad Room', 'Capacity: 4 pax', '4', 2400, 4, 'Available', 'img/Quad1.jpg', 1),
(19, 'Family Room', 'Capacity: 3-4 pax', '4', 2400, 4, 'Available', 'img/FMRoom.jpg', 1),
(20, 'Group Room', 'Capacity: 14 pax', '14', 4000, 1, 'Available', 'img/group1.jpg', 0),
(21, 'Dorm Room', 'Php 599/pax\r\nCapacity: 4 pax', '4', 599, 2, 'Available', 'img/Quad3.jpg', 2),
(22, 'Queen Room', 'Capacity: 2 pax', '2', 1950, 8, 'Available', 'img/Queen.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `walkinaddons_masterfile`
--

CREATE TABLE `walkinaddons_masterfile` (
  `walkinaddons_id` int(11) NOT NULL,
  `addon_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `walkinaddons_masterfile`
--

INSERT INTO `walkinaddons_masterfile` (`walkinaddons_id`, `addon_id`, `quantity`, `reservation_id`, `type`) VALUES
(56, 1, 15, 109, ''),
(57, 1, 3, 111, ''),
(58, 1, 2, 113, '');

-- --------------------------------------------------------

--
-- Table structure for table `walkinbilling_masterfile`
--

CREATE TABLE `walkinbilling_masterfile` (
  `walkinbilling_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `balance` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `walkinbilling_masterfile`
--

INSERT INTO `walkinbilling_masterfile` (`walkinbilling_id`, `code`, `balance`, `total`) VALUES
(1, '4', 3900, 3900),
(2, '5K2VY8HU6Z', 15600, 15600),
(3, '5CRFPBN4S9', 1922, 1950),
(4, 'JNZ78NDJB2', 0, 19200),
(5, '6Z2ICH44NG', 0, 4800),
(6, 'CLNUM2JGWS', 3800, 4800),
(7, '31Z4G04L6L', 1800, 4800),
(8, '8AC011O9NS', 8000, 8000),
(9, 'D0V5RC0VCH', 3900, 3900),
(10, '0H0JYNTSX9', 864, 1950),
(11, 'QJSP68ECHP', 11239, 11249),
(12, '7J68I2796E', 0, 0),
(13, '6TU8IC5F2J', 0, 0),
(14, 'DMH42CYX75', 1950, 1950),
(15, '5JRTKQGV7C', 0, 0),
(16, '14UCDWC7GA', 0, 0),
(17, 'PH3ZQTU6NW', 0, 0),
(18, 'E1VVEUJ6Z4', 1950, 1950),
(19, 'V7B3YDL2ZS', 3900, 3900),
(20, 'CYM2QS9TB4', 0, 0),
(21, '19FLTMJNG8', 0, 0),
(22, 'U88WES8QVV', 7800, 7800),
(23, 'WSATMUPH0G', 15600, 15600),
(24, '6T0F3Y31BS', 0, 15600),
(25, 'Y5YBPCC0FX', 9600, 9600),
(26, 'Y8SNSZ7M4S', 17400, 17400),
(27, 'T8ZAW3X6DO', 3900, 3900);

-- --------------------------------------------------------

--
-- Table structure for table `walkinreservation_masterfile`
--

CREATE TABLE `walkinreservation_masterfile` (
  `reservation_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `checkindate` date NOT NULL,
  `checkoutdate` date NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `code` varchar(10) NOT NULL,
  `balance` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `walkinreservation_masterfile`
--

INSERT INTO `walkinreservation_masterfile` (`reservation_id`, `room_id`, `checkindate`, `checkoutdate`, `firstname`, `lastname`, `code`, `balance`, `total`, `email`, `password`, `quantity`, `status`) VALUES
(109, 17, '2018-02-27', '2018-02-28', 'alyanna', 'cantos', '6T0F3Y31BS', 21600, 21600, 'alyannamariz02@gmail.com', '$2y$10$89qhsPt58PHUDnqEJMMQdOBmqBaTlaD4HOeOKhyeMr/G9/TbXFoJy', 8, 'Checkout'),
(110, 18, '2018-02-28', '2018-03-01', 'Anton', 'Umali', 'Y5YBPCC0FX', 9600, 9600, 'antonioumali@gmail.com', '$2y$10$L8gTHvwe.IIWzM5iU9sSzuCq5ViYSLLyX2coQAwUM6cRUo9MwDzHC', 4, 'Pending'),
(111, 17, '2018-07-31', '2018-08-04', 'Aaaa', 'Aaa', 'Y8SNSZ7M4S', 9000, 9000, 'aaaa@gmail.com', '$2y$10$i/L18j18vG/iA.lLWrxLL.I6o37ZS7OBqGGvjnf40JbvRSwYJDmu6', 1, 'Pending'),
(112, 18, '2018-07-31', '2018-08-04', 'Aaaa', 'Aaa', 'Y8SNSZ7M4S', 9600, 9600, 'aaaa@gmail.com', '$2y$10$i/L18j18vG/iA.lLWrxLL.I6o37ZS7OBqGGvjnf40JbvRSwYJDmu6', 1, 'Pending'),
(113, 17, '2018-08-11', '2018-08-12', 'aya', 'aya', 'T8ZAW3X6DO', 4700, 4700, 'aya@gmail.com', '$2y$10$eq5J/CcgwGlkFRO/UlHv9.Eu.4HbwtoOQxGfYvOBqto4XbttJwcv6', 2, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `walkinrooms_masterfile`
--

CREATE TABLE `walkinrooms_masterfile` (
  `walkinrooms_id` int(11) NOT NULL,
  `walkinrooms_name` varchar(30) NOT NULL,
  `room_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `walkinrooms_masterfile`
--

INSERT INTO `walkinrooms_masterfile` (`walkinrooms_id`, `walkinrooms_name`, `room_id`) VALUES
(25, 'Queen Room1', 16),
(26, 'Queen Room2', 16),
(27, 'Queen Room3', 16),
(28, 'Queen Room4', 16),
(29, 'Queen Room5', 16),
(30, 'Queen Room6', 16),
(31, 'Queen Room7', 16),
(32, 'Queen Room8', 16),
(33, 'Twin Queen Room1', 17),
(34, 'Twin Queen Room2', 17),
(35, 'Twin Queen Room3', 17),
(36, 'Twin Queen Room4', 17),
(37, 'Twin Queen Room5', 17),
(38, 'Twin Queen Room6', 17),
(39, 'Twin Queen Room7', 17),
(40, 'Twin Queen Room8', 17),
(41, 'Quad Room1', 18),
(42, 'Quad Room2', 18),
(43, 'Quad Room3', 18),
(44, 'Family Room1', 19),
(45, 'Family Room2', 19),
(46, 'Family Room3', 19),
(47, 'Group Room1', 20),
(48, 'Dorm Room1', 21),
(49, 'Dorm Room2', 21),
(50, 'Queen Room1', 22),
(51, 'Queen Room2', 22),
(52, 'Queen Room3', 22),
(53, 'Queen Room4', 22),
(54, 'Queen Room5', 22),
(55, 'Queen Room6', 22),
(56, 'Queen Room7', 22),
(57, 'Queen Room8', 22);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addons_masterfile`
--
ALTER TABLE `addons_masterfile`
  ADD PRIMARY KEY (`Addon_ID`);

--
-- Indexes for table `adminuser_masterfile`
--
ALTER TABLE `adminuser_masterfile`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `assignedroom_masterfile`
--
ALTER TABLE `assignedroom_masterfile`
  ADD PRIMARY KEY (`assignedroom_id`);

--
-- Indexes for table `billing_masterfile`
--
ALTER TABLE `billing_masterfile`
  ADD PRIMARY KEY (`billing_id`);

--
-- Indexes for table `discount_masterfile`
--
ALTER TABLE `discount_masterfile`
  ADD PRIMARY KEY (`discount_ID`);

--
-- Indexes for table `financialreports_masterfile`
--
ALTER TABLE `financialreports_masterfile`
  ADD PRIMARY KEY (`financialreport_id`);

--
-- Indexes for table `guestaddons_masterfile`
--
ALTER TABLE `guestaddons_masterfile`
  ADD PRIMARY KEY (`guestaddon_id`);

--
-- Indexes for table `guest_masterfile`
--
ALTER TABLE `guest_masterfile`
  ADD PRIMARY KEY (`guest_ID`);

--
-- Indexes for table `proofofpayment_masterfile`
--
ALTER TABLE `proofofpayment_masterfile`
  ADD PRIMARY KEY (`proofofpayment_id`);

--
-- Indexes for table `receipts_masterfile`
--
ALTER TABLE `receipts_masterfile`
  ADD PRIMARY KEY (`receipts_id`);

--
-- Indexes for table `reports_masterfile`
--
ALTER TABLE `reports_masterfile`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `reservationreports_masterfile`
--
ALTER TABLE `reservationreports_masterfile`
  ADD PRIMARY KEY (`reservereports_id`);

--
-- Indexes for table `reservation_masterfile`
--
ALTER TABLE `reservation_masterfile`
  ADD PRIMARY KEY (`reservation_id`);

--
-- Indexes for table `room_masterfile`
--
ALTER TABLE `room_masterfile`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `walkinaddons_masterfile`
--
ALTER TABLE `walkinaddons_masterfile`
  ADD PRIMARY KEY (`walkinaddons_id`);

--
-- Indexes for table `walkinbilling_masterfile`
--
ALTER TABLE `walkinbilling_masterfile`
  ADD PRIMARY KEY (`walkinbilling_id`);

--
-- Indexes for table `walkinreservation_masterfile`
--
ALTER TABLE `walkinreservation_masterfile`
  ADD PRIMARY KEY (`reservation_id`);

--
-- Indexes for table `walkinrooms_masterfile`
--
ALTER TABLE `walkinrooms_masterfile`
  ADD PRIMARY KEY (`walkinrooms_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addons_masterfile`
--
ALTER TABLE `addons_masterfile`
  MODIFY `Addon_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `adminuser_masterfile`
--
ALTER TABLE `adminuser_masterfile`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `assignedroom_masterfile`
--
ALTER TABLE `assignedroom_masterfile`
  MODIFY `assignedroom_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1298;

--
-- AUTO_INCREMENT for table `billing_masterfile`
--
ALTER TABLE `billing_masterfile`
  MODIFY `billing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `discount_masterfile`
--
ALTER TABLE `discount_masterfile`
  MODIFY `discount_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `financialreports_masterfile`
--
ALTER TABLE `financialreports_masterfile`
  MODIFY `financialreport_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `guestaddons_masterfile`
--
ALTER TABLE `guestaddons_masterfile`
  MODIFY `guestaddon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT for table `guest_masterfile`
--
ALTER TABLE `guest_masterfile`
  MODIFY `guest_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `proofofpayment_masterfile`
--
ALTER TABLE `proofofpayment_masterfile`
  MODIFY `proofofpayment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `receipts_masterfile`
--
ALTER TABLE `receipts_masterfile`
  MODIFY `receipts_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `reports_masterfile`
--
ALTER TABLE `reports_masterfile`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservationreports_masterfile`
--
ALTER TABLE `reservationreports_masterfile`
  MODIFY `reservereports_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `reservation_masterfile`
--
ALTER TABLE `reservation_masterfile`
  MODIFY `reservation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT for table `room_masterfile`
--
ALTER TABLE `room_masterfile`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `walkinaddons_masterfile`
--
ALTER TABLE `walkinaddons_masterfile`
  MODIFY `walkinaddons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `walkinbilling_masterfile`
--
ALTER TABLE `walkinbilling_masterfile`
  MODIFY `walkinbilling_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `walkinreservation_masterfile`
--
ALTER TABLE `walkinreservation_masterfile`
  MODIFY `reservation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `walkinrooms_masterfile`
--
ALTER TABLE `walkinrooms_masterfile`
  MODIFY `walkinrooms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

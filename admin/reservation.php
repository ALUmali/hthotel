<?php
 include '../config/db.php';
    if(isset($_POST['walkincheckin'])){
      echo "<script> alert('Walkin has been checked in.')</script>";
      $code = $_POST['code'];
      $updateWalkin = $conn->query("UPDATE walkinreservation_masterfile SET status = 'Checkin' WHERE code = $code");
      // mysqli_query($conn, "UPDATE walkinreservation_masterfile SET status = 'Checkin' WHERE code = '{$_POST['code']}'") or die(mysqli_error($conn));
      $fetchwalkin = mysqli_query($conn, "SELECT * FROM walkinreservation_masterfile WHERE code = '{$_POST['code']}'") or die(mysqli_error($conn));
      $walkin = mysqli_fetch_assoc($fetchwalkin);
      $currentDay = date("Y-m-d H:i:s");
      mysqli_query($conn, "INSERT INTO reservationreports_masterfile (reservation_id, created_at, updated_at, type) VALUES({$walkin['reservation_id']}, '{$currentDay}', '{$currentDay}', 'walkin')") or die(mysqli_error($conn));
    }else if(isset($_POST['walkincheckout'])){
      echo "<script>alert('Walkin has been checked out')</script>";
      mysqli_query($conn, "UPDATE walkinreservation_masterfile SET status = 'Checkout' WHERE code ='{$_POST['code']}'") or die(mysqli_error($conn));
      $fetchwalkin = mysqli_query($conn, "SELECT * FROM walkinreservation_masterfile WHERE code = '{$_POST['code']}") or die(mysqli_error($conn));
      $walkin = mysqli_fetch_assoc($fetchwalkin);
      mysqli_query($conn, "DELETE FROM assignedroom_masterfile WHERE code = '{$_POST['code']}'") or die(mysqli_error($conn));
      mysqli_query($conn, "DELETE FROM walkinaddons_masterfile WHERE reservation_id = {$walkin['reservation_id']}") or die(mysqli_error($conn));
    }
    $_POST = array();
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">
    <?php include 'layout/navbar.php';?>
     

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Reservations</h1>
                    </div>
                    <!-- Start Here -->
                    <div class ='card'>
                      <div class ='card-body' style ='padding:2px'>
                        <div class = 'row'>
                          <div class ='col-md-6'>
                              <div class ='card card-body' id ='onlinecard' align ='center' style ='background-color:	#DC143C; padding: 14px;'>
                                <a href ='#' id = 'online' style ='color:white'><b>Online reservation</b></a>
                              </div>
                          </div>
                          <div class ='col-md-6'>
                              <div class ='card card-body' id ='walkincard' align ='center' style = 'background-color: #FA8072; padding: 14px;'>
                                <a href ='#' id ='walkin' style ='color:black'>Walk-in</a>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id ='walkin1' style ='display:none'>
                      <table id ='walkintable' class ='table table-striped' >
                          <thead>
                              <th>Reservation code</th>
                              <th>Full name </th>
                              <th>Room assigned</th>
                              <th>Addons</th>
                              <th>Status</th>
                              <th>Action</th>
                          </thead>
                          <tbody>
                              <?php 
                              $fetchwalkin = mysqli_query($conn, "SELECT *, walkinbilling_masterfile.balance as wbalance, walkinbilling_masterfile.total as wtotal FROM walkinbilling_masterfile JOIN walkinreservation_masterfile ON walkinbilling_masterfile.code = walkinreservation_masterfile.code WHERE walkinreservation_masterfile.status != 'Checkout'");
                              while($row = mysqli_fetch_assoc($fetchwalkin))
                              { 
                                $fetchassignedrooms = mysqli_query($conn, "SELECT * FROM assignedroom_masterfile JOIN walkinrooms_masterfile ON assignedroom_masterfile.room_id = walkinrooms_masterfile.walkinrooms_id WHERE assignedroom_masterfile.code = '{$row['code']}'");
                                $assignedroom = array();
                                $assignedaddon = array();
                                $fetchassignaddons = mysqli_query($conn, "SELECT * FROM walkinaddons_masterfile JOIN addons_masterfile ON walkinaddons_masterfile.addon_id = addons_masterfile.Addon_ID WHERE walkinaddons_masterfile.reservation_id = {$row['reservation_id']}") or die(mysqli_error($conn));
                                while($assignedaddons = mysqli_fetch_assoc($fetchassignaddons)){
                                    $assignedaddon[] = $assignedaddons['Addon_name'];
                                }
                                while($assignedrooms = mysqli_fetch_assoc($fetchassignedrooms)){
                                  $assignedroom[] = $assignedrooms['walkinrooms_name'];
                                }
                              ?>
                              <tr>
                                  <td><?=$row['code']?></td>
                                  <td><?="{$row['firstname']} {$row['lastname']}"?></td>
                                  <td><?=implode(", ", array_unique($assignedroom))?></td>
                                  <td><?=implode(", ", array_unique($assignedaddon))?></td>
                                  <td><?=$row['status']?></td>
                                  <td>
                                      <form method ='post'>
                                          <?php 
                                          $checkindisabled = 'disabled';
                                          $checkoutdisabled = 'disabled';
                                          if($row['status'] == 'Approved'){
                                              $checkindisabled = '';
                                          }
                                          if($row['status'] =='Checkin' && $row['wbalance'] == 0){
                                              $checkoutdisabled ='';
                                          }
                                          ?>
                                          <input type ='hidden' name = 'code' value = '<?=$row ['code']?>'/>
                                          <input type = 'submit' class ='btn btn-success btn-block' <?=$checkindisabled ?> value ='Check in' name ='walkincheckin'/>
                                          <input type ='submit' class = 'btn btn-warning btn-block' <?= $checkoutdisabled ?> style ='color:white' value ='Check out' name ='walkincheckout'/>
                                      </form>
                                  </td>
                              </tr>
                              <?php } ?>
                          </tbody>
                          <tfoot>
                              
                          </tfoot>
                      </table>
                    </div>
                    <div id = 'online1' >
                      <table id ='thisTable' class ='table table-striped display dataTable table-responsive'>
                        <thead>
                          <tr>
                            <th>Reservation ID</th>
                            <th>Guest ID</th>
                            <th>Guest name</th>
                            <th>Room Name</th>
                            <th>Check in date</th>
                            <th>Check out date</th>
                            <th>Number of Guest</th>
                            <th>Room number</th>
                            <th>Room assigned</th>
                            <th>Assigned addons</th>
                            <th>Status</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $fetchallreservation = mysqli_query($conn, "SELECT *, reserve.room_number as reserve_room FROM reservation_masterfile as reserve JOIN room_masterfile as room on reserve.room_id = room.room_id JOIN guest_masterfile on guest_masterfile.guest_ID = reserve.guest_id WHERE reserve.status != 'Void' AND reserve.status != 'Checkout'") or die(mysqli_error($conn));
                          $currentTime = date("Y-m-d");
                          while($row = mysqli_fetch_assoc($fetchallreservation)){ 
                            $fetchassignedrooms = mysqli_query($conn, "SELECT * FROM assignedroom_masterfile JOIN walkinrooms_masterfile ON assignedroom_masterfile.room_id = walkinrooms_masterfile.walkinrooms_id WHERE assignedroom_masterfile.code = '{$row['reservation_code']}'");
                            $assignedroom = array();
                            $assignedaddon = array();
                            $fetchassignaddons = mysqli_query($conn, "SELECT * FROM guestaddons_masterfile JOIN addons_masterfile ON guestaddons_masterfile.addons_id = addons_masterfile.Addon_ID WHERE guestaddons_masterfile.reservation_id = {$row['reservation_id']}") or die(mysqli_error($conn));
                            while($assignedaddons = mysqli_fetch_assoc($fetchassignaddons)){
                                $assignedaddon[] = $assignedaddons['Addon_name'];
                            }
                            while($assignedrooms = mysqli_fetch_assoc($fetchassignedrooms)){
                              $assignedroom[] = $assignedrooms['walkinrooms_name'];
                            }

                            ?>
                            <tr>
                              <td id = 'reservation-id'><?=$row['reservation_id']?></td>
                              <td id = 'guest-id' ><?= $row['guest_id'] ?></td>
                              <td><?= "{$row['guest_firstname']} {$row['guest_lastname']}"?></td>
                              <td id = 'room-id' ><?= $row['room_type'] ?></td>
                              <td id = 'checkin' ><?= $row['checkindate'] ?></td>
                              <td id = 'checkout' ><?= $row['checkoutdate'] ?></td>
                              <td id = 'number-guest'><?= $row['number_guest']?></td>
                              <td id = 'room-number'><?= $row['reserve_room'] ?></td>
                              <td><?= implode(", ", array_unique($assignedroom))?></td>
                              <td><?= implode(", ", array_unique($assignedaddon))?></td>
                              <td><?=$row['status']?></td>
                              <td><form id = 'deletereservation'>
                                <?php $checkIndisabled = 'disabled';
                                $checkOutdisabled = 'disabled';
                                if($row['status'] == 'Checkin')
                              //if(true)
                                  $checkOutdisabled = '';
                                  
                              // if($row['checkindate'] <= $currentDay & ($row['status'] != 'Checkin' && $row['status'] != 'Checkout' ))
                              if($row['status'] != 'Checkin' && $row['status'] != 'Checkout' )
                                //if(true)
                                  $checkIndisabled = '';
                                ?>
                                <input type ='hidden' <?=$row['reservation_code']?> name ='code'/>
                                <input type ='hidden' <?=$checkIndisabled?> name ='checkin'/>
                                <input type ='submit'<?=$checkIndisabled?> name ='checkIn' class ='btn btn-success btn-block' style ='margin-bottom:10px' value ='Check in'/>
                                <input type ='hidden' <?=$checkOutdisabled?> name = 'checkout' value ='Checkout'/>
                                <input type ='submit' <?=$checkOutdisabled?> name ='checkout' class ='btn btn-warning btn-block' value ='Check out' style ='margin-bottom:10px; color:white'/>
                                <input type="hidden" name="t_id" value="<?= $row['reservation_id'] ?>">
                                <input type ='hidden' name ='decrease' value ='<?=floor(($row['number_guest']-1)/$row['room_capacity']) ?>'/>
                              </form></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot></tfoot>
                      </table>
                    </div>
                    <footer class="sticky-footer">
                      <div class="container">
                        <div class="text-center">
                          <h3>
                             <?php
                                echo "Today is " . date("Y/m/d") . "<br>";
                                echo " " . date("h:i:sa");
                              ?>
                          </h3>
                        </div>
                      </div>
                    </footer>      
                    <!-- Scroll to Top Button-->
                    <a class="scroll-to-top rounded" href="#page-top">
                            <i class="fa fa-angle-up"></i>
                          </a>

                          <!-- Logout Modal-->
                          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                  </button>
                                </div>
                                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                                <div class="modal-footer">
                                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                  <a class="btn btn-primary" href="login.php">Logout</a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- Edit Modal -->
                          <div class="modal fade" id ='editreservation' tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title">Edit Guest Reservation</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <form id="formEditRoom" enctype="multipart/form-data" method ='post' aria-location = '../ajax/getreservedrooms.php' action = '../ajax/editreservation.php' aria-delete = '../ajax/cancelreservation.php'>
                                    <div class='container-fluid'>
                                      <div class='form-group'>
                                        <table class='table table-bordered' id='dataTable' width='100%' cellspacing='0'>
                                          <label for='RoomType'>Check in</label><br>
                                          <input required class='form-control' name = 'checkin' id = 'checkInDate' type ='text'>
                                          <div class='form-group'>
                                            <label for='roomRate'>Check out</label><br>
                                            <input required class='form-control' name = 'checkout'  id = 'checkOutDate' type='text'>
                                          </div>
                                          <div class='form-group'>
                                            <label for='roomNumber'>Room Type</label><br>
                                            <select class ='form-control' name ='roomtype' id ='roomtype'>
                                              <?php $fetchrooms = mysqli_query($conn, "SELECT * FROM room_masterfile");
                                              while($row = mysqli_fetch_assoc($fetchrooms)){
                                                echo "<option class ='get' value = '{$row['room_id']}'>{$row['room_type']}</option>";
                                              } ?>
                                            </select>
                                          </div>
                                          <div class='form-group'>
                                            <label for='roomRate'>Room Quantity</label><br>
                                            <select id ='roomquantity' name = 'roomquantity' class ='form-control'>

                                            </select>
                                          </div>
                                          <input type ='hidden' name = 'reservationno'/>
                                          <input type ='hidden' name ='decrease'/>
                                        </table>
                                        <h5>Addons</h5>
                                        <hr/>
                                        <div class ='row' id ='addons'>

                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                    <div class="modal-footer">
                                      <input type ='hidden' name = 'roomId'>
                                      <button name = 'update' type = 'submit' class='btn btn-primary btn-block'>Update Room</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <div class="modal fade" id ='addservice' tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title">Add services</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <form  method ='post'>
                                      <div class='container-fluid'>
                                        <div class='form-group'>
                                          <div class ='row'>
                                            <div class ='form-group col-md-9'>
                                              <label>Addons</label>
                                              <select name ='addons' id ='addonsQuantity' class ='form-control'>
                                                <?php $fetchaddons = mysqli_query($conn, "SELECT * FROM addons_masterfile");
                                                while($addons = mysqli_fetch_assoc($fetchaddons)){?>
                                                <option value ='<?=$addons['Addon_ID']?>'><?=$addons['Addon_name']?></option>
                                                <?php } ?>
                                                <option value ='x' selected>None</option>
                                              </select>
                                            </div>
                                            <div class ='form-group col-md-3'>
                                              <label>Quantity</label>
                                              <select name = 'addonsqty' id = 'addonsqty' class ='form-control'>

                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                    <div class="modal-footer">
                                      <input type ='hidden' name ='checkInDate' value =''/>
                                      <input type ='hidden' name ='checkOutDate' value =''/>
                                      <input type ='hidden' name ='r_id' value =''/>
                                      <input type ='hidden' name ='guest_ud' value =''/>
                                      <input type ='submit' name ='submitservice' class ='btn btn-info btn-block'/>
                                    </div>
                                  </form>
                                  <?php
                                  if(isset($_POST['submitservice'])){
                                    if($_POST['addons'] != 'x'){
                                      $selectaddon = mysqli_query($conn, "SELECT * FROM guestaddons_masterfile WHERE reservation_id = {$_POST['r_id']} AND addons_id = {$_POST['addons']}") or die(mysqli_error($conn));
                                      if(mysqli_num_rows($selectaddon) == 0){
                                        mysqli_query($conn, "INSERT INTO guestaddons_masterfile(addons_id,reservation_id,quantity) VALUES({$_POST['addons']},{$_POST['r_id']},{$_POST['addonsqty']})") or die(mysqli_error($conn));
                                      }
                                      else{
                                        mysqli_query($conn, "UPDATE guestaddons_masterfile SET quantity = quantity + {$_POST['addonsqty']} WHERE reservation_id = {$_POST['r_id']} AND addons_id = {$_POST['addons']}") or die(mysqli_error($conn));
                                      }
                                      $fetchrate = mysqli_query($conn, "SELECT * FROM addons_masterfile WHERE Addon_ID = {$_POST['addons']}");
                                      $rate = mysqli_fetch_assoc($fetchrate);
                                      $total = $rate['Addon_rate'] * $_POST['addonsqty'];
                                    // add to billing
                                      mysqli_query($conn, "UPDATE billing_masterfile SET balance = balance + {$total}, total = total + {$total} WHERE reservation_id = {$_POST['r_id']}");
                                      echo "<script>alert('Success')</script>";
                                    }
                                  }
                                  ?>
                                </div>
                              </div>
                            </div>
                          </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    

    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>


    <!-- Custom scripts for this page-->
    <!-- DataTable-->
    <script type = 'text/javascript' src ='../js/datatables.min.js'></script> 
    <script type ='text/javascript' src = '../js/dataTables.bootstrap4.min.js'></script>
    

    <!-- Datepicker-->
    <!-- <script src="../js/jquery.datetimepicker.full.min.js"></script> -->
    <script src = '../js/edit_reservation.js'></script>
    <script>
      $(document).ready(function(){
        $('#thisTable').DataTable({
            "paging" :false
        })
        $('#walkintable').DataTable({
            "paging": false
        })
              $('#walkin').click(function(){
          $(this).css('color','white')
          $('#online').css('color','black');
          $(this).parent().css('background-color','#DC143C')
          $('#onlinecard').css('background-color','	#FA8072')
          $('#walkin1').fadeIn()
          $('#online1').fadeOut()
      })
      $('#online').click(function(){
          $(this).css('color','white')
          $('#walkin').css('color','black')
          $(this).parent().css('background-color','#DC143C')
          $('#walkincard').css('background-color','	#FA8072')
          $('#walkin1').fadeOut()
          $('#online1').fadeIn()
      })
        $('#addonsQuantity').change(function(){
          $.ajax({
            type:'POST',
            url:'../ajax/getalladdons.php',
            data:{
              checkInDate: $('input[name=checkInDate]').val(),
              checkOutDate: $('input[name=checkOutDate').val(),
              a_id: $(this).val()
            },
            success:function(html){
              var addons = parseInt(html)
              $('#addonsqty').empty()
              for(var x = 1; x<=addons; x++){
                $('#addonsqty').append(`<option value = '${x}'>${x}</option>`)
              }
            }
          })
        })
        $('.addservice').click(function(){
          $('#addonsQuantity option').last().remove()
          $('#addonsQuantity').append(`<option value ='x' selected >None</option>`)
          var checkInDate = $(this).closest('tr').find('#checkin').html()
          var checkOutDate = $(this).closest('tr').find('#checkout').html()
          var r_id = $(this).closest('tr').find('input[name=t_id]').val()
          $('input[name=r_id]').val(r_id)
          $('input[name=checkInDate').val(checkInDate)
          $('input[name=checkOutDate').val(checkOutDate)
        })
      })
      
    </script>
</body>

</html>

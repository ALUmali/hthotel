<?php
include "../db.php";

$start = $_GET['start'];
$end = $_GET['end'];
$month = $_GET['month'];

$fetchguestdiscount = $conn->query(
    "SELECT * FROM guest_masterfile WHERE COUNT >= 5");

?>

<table class ='table table-striped display dataTable table-responsive'>
<thead>
    <tr>
    <th>Guest Name</th>
    <th>Email</th>
    <th>Contact Number</th>
    <th>Country</th>
    <th>Address</th>
    </tr>
</thead>
<tbody>
<?php
while ($rows = mysqli_fetch_assoc($fetchguestdiscount)) {
    $guest_name = $rows['guest_firstname'] . " " . $rows['guest_lastname'];
    $guest_email = $rows['guest_email'];
    $guest_contactNumber = $rows['guest_contactNumber'];
    $guest_country = $rows['guest_country'];
    $guest_address = $rows['guest_address'];
    ?>
<tr>
<td><?php echo $guest_name; ?></td>
<td><?php echo $guest_email; ?></td>
<td><?php echo $guest_contactNumber; ?></td>
<td><?php echo $guest_country; ?></td>
<td><?php echo $guest_address; ?></td>
</tr>

<?php
}

?>

</tbody>
<tfoot></tfoot>
</table>

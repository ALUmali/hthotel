<?php
include "../db.php";

$start = $_GET['start'];
$end = $_GET['end'];
$month = $_GET['month'];

// echo $start;
// echo $end;

// $fetchallreservation = mysqli_query($conn, "SELECT * FROM reservationreports_masterfile");

$fetch_all_reservation = $conn->query(
    "SELECT * FROM reservationreports_masterfile
    JOIN reservation_masterfile ON reservationreports_masterfile.reservation_id = reservation_masterfile.reservation_id
    JOIN guest_masterfile ON guest_masterfile.guest_ID = reservation_masterfile.guest_id
    -- JOIN walkinreservation_masterfile ON walkinreservation_masterfile.reservation_id = guest_masterfile.reservation_id
    WHERE checkindate BETWEEN '$start' AND '$end'");    

?>
<table class ='table table-striped display dataTable table-responsive'>
<thead>
    <tr>
    <th>Guest Name</th>
    <th>Status</th>
    <th>Type</th>
    <th>Check-in</th>
    <th>Check-out</th>
    <!-- <th>Created at</th> -->
    <th>Updated at</th>
    </tr>
</thead>
<tbody>
<?php
while ($rows = mysqli_fetch_assoc($fetch_all_reservation)) {
    $guest_name = $rows['guest_firstname'] . " " . $rows['guest_lastname'];
    $reservationStatus = $rows['status'];
    $type = $rows['type'];
    $checkin = $rows['checkindate'];
    $checkout = $rows['checkoutdate'];
    // $createdAt = $rows['created_at'];
    $updatedAt = $rows['updated_at'];
    ?>
<tr>
<td><?php echo $guest_name; ?></td>
<td><?php echo $reservationStatus; ?></td>
<td><?php echo $type; ?></td>
<td><?php echo $checkin; ?></td>
<td><?php echo $checkout; ?></td>
<!-- <td><?php //echo $createdAt; ?></td> -->
<td><?php echo $updatedAt; ?></td>

</tr>

<?php
}

?>

</tbody>
<tfoot></tfoot>
</table>

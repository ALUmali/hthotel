<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">
    <?php include 'layout/navbar.php';?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Reservation Reports</h1>
                    </div>
                    <!-- Start Here -->
                    <div class ='card' style ='margin-bottom:50px'>
                        <!-- <div class ='card-header'>
                            <h3>Filter options</h3>
                        </div> -->
                        <div class ='card-body'>
                            <div class ='row'>
                                <div class="form-group col-md-4">
                                    <label>Start Date</label>
                                    <input type="date" id="start" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>End Date</label>
                                    <input type="date" id="end" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    
                                    <button class ='btn btn-success printbtn' style ='margin-top:24px' onclick="generateReport();">Search</button>
                                    <a href = 'printreport.php?category=reservation' style ='margin-top:24px' target ='_blank' class ='btn btn-success printbtn'>Print</a>    
                                </div>
                                
                        </div>
                        <!-- <div> -->
                    </div>
                       <div class="report-search">

                       </div>
                        
                        <footer class="sticky-footer">
                        <div class="container">
                            <div class="text-center">
                            <small>Copyright © HomeTown Hotel Makati</small>
                            </div>
                        </div>
                        </footer>
                        

                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <script>
    function generateReport()
        {
            var start = $("#start").val();
            var end = $("#end").val();
            $(".report-search").load(`reservationReport_search.php?&start=${start}&end=${end}&month=`);
        }
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>

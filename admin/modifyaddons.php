<?php
    include '../config/db.php';
    if(isset($_POST['updateinfo'])){

    $addonName = mysqli_real_escape_string($conn, $_POST['addonName']);

    $addonRate= mysqli_real_escape_string($conn, $_POST['addonRate']);

    $addonDescription = mysqli_real_escape_string($conn, $_POST['addonDesc']);

    $addonQuantity = mysqli_real_escape_string($conn, $_POST['addonquantity']);

    mysqli_query($conn, "UPDATE addons_masterfile SET Addon_name = '{$addonName}', Addon_rate ={$addonRate}, Addon_description ='{$addonDescription}', addon_qty = {$addonQuantity} WHERE Addon_ID = {$_POST['addon_id']}") or die(mysqli_error($conn));

    echo "<script>alert('Success')</script>";

    }

    if (isset($_POST['delete'])) {

        mysqli_query($conn, "DELETE FROM addons_masterfile WHERE Addon_ID = {$_POST['addon_id']}") or die(mysqli_error($conn));
      
        echo "<script>alert('Add-on has been deleted')location.href='modifyaddons.php'</script>";
      
      }
      
      $_POST = array();
      
      ?>
      


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">
</head>

<body>
    <div id="wrapper">
    <?php include 'layout/navbar.php';?>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Modify Add-ons</h1>
                    </div>
                    <!-- Start Here -->

                          <div class="content-wrapper">
      
      <div class="container-fluid">
     
        <h3>User Accounts</h3>
     
        <div class="table-responsive">
     
          <table class="table table-bordered table-striped" id="dataTable" align="center">
     
            <tr>
     
              <th>Add-on ID</th>
     
              <th>Add-on Name</th>
     
              <th>Add-on Rate</th>
     
              <th>Add-on Description</th>
     
              <th>Add-on Status</th>
     
              <th>Add-on Quantity</th>
     
              <th>Action</th>
     
            </tr>
     
            <?php
     
            $result = mysqli_query($conn, "SELECT * FROM addons_masterfile");
     
            while($row = mysqli_fetch_assoc($result)) {
     
             echo "<tr>
     
               <td class ='addon-id'>" . $row["Addon_ID"]. "</td>
       
               <td class ='addonName'>" . $row["Addon_name"]. "</td>
       
               <td class ='addonRate'>" . $row["Addon_rate"]. "</td>
       
               <td class ='addonDescription'>" . $row["Addon_description"] . "</td>
       
               <td class ='addonStatus'>" . $row["Addon_status"] . "</td>
       
               <td class ='addonQuantity'>{$row['Addon_qty']}
     
             </td><td>
     
             <a data-toggle ='modal' data-target = \"#editModal\" class='btn btn-primary edit' style ='color:white;margin-bottom:10px'>Edit</a>
     
             <form method = 'POST' action = 'adminusermodify.php'>
     
               <input type ='hidden' value = '{$row['Addon_ID']}' name = 'delete_id'> 
       
               <button name = 'delete' class = 'btn btn-info btn-xs edit_data' onclick = 'return confirm(\"Are you sure? \")' type = 'submit'>Delete</button>
       
             </form>
     
             </td>";
     
           }
     
           ?>
     
       </table>
     
   </div>

</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">

<div class='modal-dialog modal-lg'>

  <div class='modal-content'>

    <div class='modal-header'>

      <h4 class='modal-title'>Update account</h4>

      <button type='button' class='close' data-dismiss='modal'>&times;</button>

    </div>

    <div class='modal-body'>

      <!-- <div class='content-wrapper'> -->

        <form method ='post' >



         <input type ='hidden' name ='addon_id' />

         <div class="form-group">

          <div class="form-row">

            <div class="col-md-6">

              <label for="exampleInputName">Addon name</label>

              <input  required class="form-control" id="exampleInputName" name = "addonName" type="text" aria-describedby="nameHelp" pattern = "[a-z A-Z]+" onkeypress="return isLetter(event)">

            </div>

            <div class="col-md-6">

              <label for="exampleInputLastName">Addon Rate</label>

              <input required class="form-control" id="exampleInputLastName" name = "addonRate" type="number" aria-describedby="nameHelp" pattern = "[a-z A-Z ]+" onkeypress="return isLetter(event)">

            </div>

          </div>

        </div>

        <div class="form-group">

          <div class="form-row" style ='margin-bottom:10px'>

            <div class="col-md-6">

              <label for="exampleInputEmail1">Addon Description</label>

              <input required class="form-control" id="exampleInputEmail1" name = "addonDesc" type ='text' aria-describedby="emailHelp">

            </div>

            <div class ='col-md-6'>

             <label>Addon Quantity</label>

             <input class ='form-control' name ='addonquantity' type ='number'/>

            </div>

          </div>

          <button class="btn btn-primary btn-block" name = "updateinfo" type = "submit">Update Addon</button>

        </div>

      </form>

    <!-- </div> -->

  </div>

  <div class='modal-footer'>

    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>

  </div>

</form>

</div>

</div>

</div>

</div>

</div>

                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

 
   <!-- jQuery -->
   <script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<script src="../vendor/datepicker/jquery.datetimepicker.full.min.js"></script>

<script>

  $('.edit').click(function(){

    addon_id = $(this).closest('tr').find('.addon-id').html()

    account_id = $(this).closest('tr').find('.addonName').html()

    firstname = $(this).closest('tr').find('.addonRate').html()

    lastname = $(this).closest('tr').find('.addonDescription').html()

    email = $(this).closest('tr').find('.addonQuantity').html()

    $('input[name=addon_id]').val(addon_id)

    $('input[name=addonName]').val(account_id)

    $('input[name=addonRate]').val(firstname)

    $('input[name=addonDescription]').val(lastname)

    $('input[name=addonquantity]').val(parseInt(email))

  })

</script>
</body>

</html>

<?php
session_start();
    include 'layout/navbar.php';
    include '../config/db.php';


if (isset($_POST['submit'])) {
    $roomType = mysqli_escape_string($conn, $_POST['roomType']);
    $roomDescription = mysqli_escape_string($conn, $_POST['roomDescription']);
    $roomCapacity = mysqli_escape_string($conn, $_POST['roomCapacity']);
    $roomRate = mysqli_escape_string($conn, $_POST['roomRate']);
    $roomNumber = mysqli_escape_string($conn, $_POST['roomNumber']);
    $roomStatus = mysqli_escape_string($conn, $_POST['roomStatus']);
    $name = basename($_FILES['image_upload']['name']);
    $tmp = $_FILES['image_upload']['tmp_name'];
    move_uploaded_file($tmp,"../img/{$name}");
    $query = mysqli_query($conn, "INSERT INTO room_masterfile (room_type, room_description, room_capacity, room_rate, room_number, room_status, room_imagepath) VALUES('{$roomType}','{$roomDescription}',
      {$roomCapacity}, {$roomRate}, {$roomNumber},'{$roomStatus}','img/{$name}')") or die(mysqli_error($conn));
    $fetchnewid = mysqli_query($conn, "SELECT max(room_id) FROM room_masterfile");
    $room = mysqli_fetch_assoc($fetchnewid);
    for($i = 1; $i <= $roomNumber; $i++){
      mysqli_query($conn, "INSERT walkinrooms_masterfile(walkinrooms_name, room_id) VALUES('{$roomType}{$i}', {$room['max(room_id)']})") or die(mysqli_error($conn));
    }
    // echo "<script>window.alert('Success! Room added.');window.location.href='Roomsdelete.php';</script>";
    // $insert_result = mysqli_query($conn, "INSERT INTO room_masterfile(room_type, room_description, room_capacity, room_rate, room_number, room_status, room_imagepath) 
    // VALUES ('{$roomType}', '{$roomDescription}', {$roomCapacity}, {$roomRate}, {$roomNumber}, '{$roomStatus}', '../img/{$roomImage}')") or die (mysqli_error($conn));
}
?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">    

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Add Rooms</h1>
                    </div>
                    <!-- Start Here -->
                    <!-- <div class="container"> -->
                      <div class="card card-login mx-auto mt-5">
                        <!-- <div class="card-header">Add Rooms</div> -->
                        <div class="card-body">
                          <form action="" method = "POST" enctype="multipart/form-data">
                            <div class="form-group">
                              <label for="RoomType">Room Type</label><br>
                              <input required required class="form-control" name = "roomType" type="text" aria-describedby="emailHelp" placeholder="Room Type">
                            </div>
                            <div class="form-group">
                              <label for="asd">Room Description</label><br>
                              <textarea required rows="4" type = "text" cols="43" name="roomDescription" placeholder="Enter room description here..."></textarea>
                            </div>
                            <div class="form-group">
                              <label for="roomRate">Room Capacity</label><br>
                              <input required required class="form-control" name = "roomCapacity" type="number" placeholder="Room Capacity">
                            </div>
                            <div class="form-group">
                              <label for="roomRate">Room Rate per night</label><br>
                              <input required required class="form-control" name = "roomRate" type="number" placeholder="Room Rate">
                            </div>
                            <div class="form-group">
                              <label for="roomNumber">Room Number</label><br>
                              <input required required class="form-control" name = "roomNumber" type="number" placeholder="Room Number">
                            </div>
                            <div class="form-group">
                              <label for="roomStatus">Room Status &nbsp&nbsp</label>
                              <select required name = "roomStatus">
                                <option value = "Available" selected = "selected">Available</option>
                                <option value = "UnderMaintenance">Reserved</option>
                                <option value = "Occupied">Occupied</option> 	
                              </select>            	
                            </div>
                            <div class='form-group'>
                              <label for ='image_upload'>Image</label><br>
                              <input required type ='file' name = 'image_upload'>
                            </div>
                            </div>
                            <input type = 'submit' name = "submit" class="btn btn-primary btn-block" />
                            <div class="text-center">
                          <a class="d-block small mt-3" href="AdminPanel.php">Go Back</a>
                        </div>
                          </form> 
                        </div>
                      </div>
                    <!-- </div> -->

                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
